import Vue from 'vue';
import AQL from './AQL.vue';

var highmedPluginAQL = {
  install: function(Vue, options) {
    // const {
    //   store
    // } = options
    // if (!store) {
    //   throw new Error("Please provide vuex store.");
    // }

    // store.registerModule({
    //   states,
    //   mutations,
    //   actions
    // });

    Vue.component('highmed-plugin-AQL', AQL);
  }
};

export default highmedPluginAQL;